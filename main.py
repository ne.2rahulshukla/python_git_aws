from flask import Flask
from flask_restful import Resource, Api
import logging

app = Flask(__name__)
api = Api(app)

logging.basicConfig(filename='main.log', encoding='utf-8', level=logging.DEBUG)
logging.debug('This message realted to the debug')
logging.info('this message is realted to info')
logging.warning('this message is realted to warning')
logging.error('this message is related to error')


class HelloWorld(Resource):
    def get(self):
        return {'hello': 'world'}

class Dashboard(Resource):
    def get(self):
        dict = {'a': 100, 'b': 200, 'c': 300}
        list = []
        for i in dict:
            list.append(dict[i])
        final = sum(list)
        print("Sum of list:", final)
        return {'board': 'this is newley on boarded dashboard'}

class TakeInput(Resource):
    def get(self, integer):
        try:
            integer = int(integer)
            num = 0
            for i in range(0,integer+1):
                num = num + i
            print("final number is", num)
            return {"final number is" : num}
        except Exception as e:
            print("the error is:", e)

api.add_resource(HelloWorld, '/')
api.add_resource(Dashboard, '/dash')
api.add_resource(TakeInput, '/input/<string:integer>')

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8001)